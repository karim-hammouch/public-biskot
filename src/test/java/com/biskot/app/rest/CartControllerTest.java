package com.biskot.app.rest;

import com.biskot.IntegrationTest;
import com.biskot.app.contract.model.AddItemRequest;
import com.biskot.domain.model.Cart;
import com.biskot.domain.service.CartService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.stream.LongStream;

@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureWebTestClient
class CartControllerTest {

    private static final double DEFAULT_TOTAL_PRICE = 98;
    private static final Long DEFAULT_ID = 1L;
    private static final Long ERROR_ID = 44L;
    private static final Long PRODUCT_ID = 4L;
    private static final Integer DEFAULT_QUANTITY = 2;
    private static final Integer ONE_QUANTITY = 1;
    private static final Integer ERROR_QUANTITY = 2000;
    private static final Integer ZERO_QUANTITY = 0;


    @Value("${biskot.cart.max}")
    private Double maxCartAmount;

    @Value("${biskot.product.max}")
    private Integer maxProductCount;

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private CartService cartService;

    private Cart cart;

    private static Cart createEntity() {
        Cart cart = new Cart();
        cart.setTotalPrice(DEFAULT_TOTAL_PRICE);
        return cart;
    }

    @BeforeAll
    static void beforeAll() {

    }

    @BeforeEach
    void setUp() {
        cartService.createCart();
    }

    @AfterEach
    void afterEach(){
        cartService.initDatabase();
    }

    @Test
    void createCartTest() {
        webTestClient.post()
                .uri("/carts")
                .exchange()
                .expectStatus().isOk()
                .expectHeader().valueMatches("message", "cart created")
                .expectBody().isEmpty();
    }

    @Test
    void getCartTest() {
        webTestClient.get()
                .uri("/carts/{id}", DEFAULT_ID)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody().jsonPath("$.id").isNotEmpty();
    }

    @Test
    void getCartTestNotFound() {
        webTestClient.get()
                .uri("/carts/{id}", ERROR_ID)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    void addItemToCartTestOK() {
        webTestClient.put()
                .uri("/carts/{id}/items", DEFAULT_ID)
                .bodyValue(new AddItemRequest().productId(DEFAULT_ID).quantity(DEFAULT_QUANTITY))
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    void addItemToCartTestBusinessErrorStock() {
        webTestClient.put()
                .uri("/carts/{id}/items", DEFAULT_ID)
                .bodyValue(new AddItemRequest().productId(DEFAULT_ID).quantity(ERROR_QUANTITY))
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody().jsonPath("$.message").isEqualTo(String.format("Quantity %s asked for the product %s is not available in stock", ERROR_QUANTITY, DEFAULT_ID));
    }

    @Test
    void addItemToCartTestBusinessErrorQuantityZero() {
        webTestClient.put()
                .uri("/carts/{id}/items", DEFAULT_ID)
                .bodyValue(new AddItemRequest().productId(DEFAULT_ID).quantity(ZERO_QUANTITY))
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody().jsonPath("$.message").isEqualTo(String.format("Quantity value %s not valid", ZERO_QUANTITY));
    }

    @Test
    void addItemToCartTestBusinessErrorProductNotFound() {
        webTestClient.put()
                .uri("/carts/{id}/items", DEFAULT_ID)
                .bodyValue(new AddItemRequest().productId(ERROR_ID).quantity(DEFAULT_QUANTITY))
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    void addItemToCartTestBusinessErrorAmountExceed() {
        webTestClient.put()
                .uri("/carts/{id}/items", DEFAULT_ID)
                .bodyValue(new AddItemRequest().productId(PRODUCT_ID).quantity(DEFAULT_QUANTITY))
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody().jsonPath("$.message").isEqualTo(String.format("cart max amount %s has been exceed", maxCartAmount));
    }

    @Test
    void addItemToCartTestBusinessErrorNumberDifferentProduct() {
        LongStream.range(1, 4).forEach(counter -> cartService.addItemToCart(DEFAULT_ID, counter, ONE_QUANTITY));
        webTestClient.put()
                .uri("/carts/{id}/items", DEFAULT_ID)
                .bodyValue(new AddItemRequest().productId(PRODUCT_ID).quantity(ONE_QUANTITY))
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody().jsonPath("$.message").isEqualTo(String.format("product quota %s has been exceed", maxProductCount));
    }
}