package com.biskot.infra.repository;

import com.biskot.domain.mapper.CartMapper;
import com.biskot.domain.model.Cart;
import com.biskot.domain.model.Product;
import com.biskot.domain.service.exception.NotFoundException;
import com.biskot.domain.spi.CartPersistencePort;
import com.biskot.infra.gateway.ProductGateway;
import com.biskot.infra.repository.entity.CartEntity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

@Repository
public class InMemoryCartRepository implements CartPersistencePort {

    private final CartMapper cartMapper;
    private final ProductGateway productGateway;
    private static AtomicLong cartSequenceGenerator = new AtomicLong(0L);

    private Set<CartEntity> datasource = new HashSet<>();

    public InMemoryCartRepository(CartMapper cartMapper, ProductGateway productGateway) {
        this.cartMapper = cartMapper;
        this.productGateway = productGateway;
    }

    @Override
    public Optional<Cart> getCart(long id) {
        CartEntity element = datasource.stream().filter(item -> item.getId() == id).findFirst().orElseThrow(NotFoundException::new);
        Cart result = cartMapper.toDto(element);
        result.getItems().forEach(i -> {
            Product product = productGateway.getProduct(i.getProduct().getId());
            i.setProduct(product);
        });
        return Optional.ofNullable(result);
    }

    @Override
    public void saveCart(Cart cart) {
        if(datasource == null) {
            datasource = new HashSet<>();
        }
        CartEntity cartEntity = cartMapper.toEntity(cart);
        datasource.remove(cartEntity);
        datasource.add(cartEntity);
    }

    public void initDataSource() {
        datasource.clear();
        cartSequenceGenerator = new AtomicLong();
    }

    public Long getNextSequenceValue() {
        return cartSequenceGenerator.incrementAndGet();
    }
}
