package com.biskot.infra.repository.entity;

public class ItemEntity {

    private Long id;
    private Long productId;
    private Integer quantityAsked;
    private Double lineAmount;

    public ItemEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Integer getQuantityAsked() {
        return quantityAsked;
    }

    public void setQuantityAsked(Integer quantityAsked) {
        this.quantityAsked = quantityAsked;
    }

    public Double getLineAmount() {
        return lineAmount;
    }

    public void setLineAmount(Double lineAmount) {
        this.lineAmount = lineAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        com.biskot.infra.repository.entity.ItemEntity item = (com.biskot.infra.repository.entity.ItemEntity) o;

        return id.equals(item.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", product=" + productId +
                ", quantityAsked=" + quantityAsked +
                ", lineAmount=" + lineAmount +
                '}';
    }
}
