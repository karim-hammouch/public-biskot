package com.biskot.infra.gateway;

import com.biskot.domain.mapper.ProductMapper;
import com.biskot.domain.model.Product;
import com.biskot.domain.service.exception.BusinessServiceException;
import com.biskot.domain.service.exception.NotFoundException;
import com.biskot.domain.spi.ProductPort;
import com.biskot.infra.gateway.payload.ProductResponse;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

@Service
public class ProductGateway implements ProductPort {

    private final ProductMapper productMapper;

    @Value("${biskot.gateway.mock.enabled}")
    private boolean isMockContext;

    private static final Duration REQUEST_TIMEOUT = Duration.ofSeconds(3);

    private Cache<Long, ProductResponse> cacheProducts;

    private final WebClient localApiClient;

    public ProductGateway(WebClient localApiClient, ProductMapper productMapper) {
        this.localApiClient = localApiClient;
        this.productMapper = productMapper;
        this.cacheProducts = CacheBuilder.newBuilder()
                .maximumSize(10)
                .expireAfterWrite(5, TimeUnit.MINUTES)
                .build();
    }

    public Product getProduct(long productId) {
        if(!isMockContext) {
            throw new BusinessServiceException("The real service is not yet implemented, " +
                    "to use the mock server please set the ecommerce.gateway.mock to true in the conf file");
        }
        ProductResponse cacheResponse = cacheProducts.getIfPresent(productId);
        if(cacheResponse != null) {
            return productMapper.toEntity(cacheResponse);
        }

        return productMapper.toEntity(localApiClient
                .get()
                .uri("/products/" + productId)
                .retrieve()
                .bodyToMono(ProductResponse.class)
                .onErrorResume(e -> Mono.error(new NotFoundException("product with id %s not found in mock server", productId)))
                .doOnNext(item -> cacheProducts.put(productId, item))
                .log()
                .block(REQUEST_TIMEOUT));
    }

}
