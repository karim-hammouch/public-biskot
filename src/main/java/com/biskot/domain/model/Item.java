package com.biskot.domain.model;

import java.io.Serializable;

public class Item implements Serializable {

    private Long id;
    private Product product;
    private Integer quantityAsked;
    private double lineAmount;

    public Item() {
    }

    public Long getId() {
        return id;
    }

    public Item id(Long id) {
        this.id = id;
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public Item product(Product product) {
        this.product = product;
        return this;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getQuantityAsked() {
        return quantityAsked;
    }

    public Item quantityAsked(Integer quantityAsked) {
        this.quantityAsked = quantityAsked;
        return this;
    }

    public void setQuantityAsked(Integer quantityAsked) {
        this.quantityAsked = quantityAsked;
    }

    public double getLineAmount() {
        return lineAmount;
    }

    public Item lineAmount(double lineAmount) {
        this.lineAmount = lineAmount;
        return this;
    }

    public void setLineAmount(double lineAmount) {
        this.lineAmount = lineAmount;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", product=" + product +
                ", quantityAsked=" + quantityAsked +
                ", lineAmount=" + lineAmount +
                '}';
    }
}
