package com.biskot.domain.model;

import java.io.Serializable;

public class Product implements Serializable {

    private Long id;
    private String label;
    private double unitPrice;
    private short quantityAvailable;

    public Product() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public short getQuantityAvailable() {
        return quantityAvailable;
    }

    public void setQuantityAvailable(short quantityAvailable) {
        this.quantityAvailable = quantityAvailable;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", label='" + label + '\'' +
                ", unitPrice=" + unitPrice +
                ", quantityAvailable=" + quantityAvailable +
                '}';
    }
}
