package com.biskot.domain.service.exception;

public class BusinessServiceException extends RuntimeException {

    public BusinessServiceException(String message, Object... args) {
        super(String.format(message, args));
    }
}
