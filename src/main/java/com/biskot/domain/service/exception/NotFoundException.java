package com.biskot.domain.service.exception;

public class NotFoundException extends RuntimeException {

    public NotFoundException() {
        super();
    }

    public NotFoundException(String message, Object... args) {
        super(String.format(message, args));
    }
}
