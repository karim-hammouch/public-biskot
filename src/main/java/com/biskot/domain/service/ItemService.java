package com.biskot.domain.service;

import com.biskot.domain.model.Item;

public interface ItemService {

    Item createItem(Long productId, Integer quantity);
}
