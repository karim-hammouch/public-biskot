package com.biskot.domain.service;

import com.biskot.domain.model.Item;
import com.biskot.domain.model.Product;
import com.biskot.domain.service.exception.NotFoundException;
import com.biskot.domain.service.exception.BusinessServiceException;
import com.biskot.infra.gateway.ProductGateway;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicLong;

@Service
public class ItemServiceImpl implements ItemService {

    private final ProductGateway productGateway;
    private static AtomicLong itemSequenceGenerator = new AtomicLong(0L);

    public ItemServiceImpl(ProductGateway productGateway) {
        this.productGateway = productGateway;
    }

    @Override
    public Item createItem(Long productId, Integer quantity) {
        Product product = productGateway.getProduct(productId);
        businessRulesValidation(productId, quantity, product);
        return new Item()
                .id(getNextSequenceValue())
                .product(product)
                .quantityAsked(quantity)
                .lineAmount(calculateLineAmount(product, quantity));
    }

    private void businessRulesValidation(Long productId, Integer quantity, Product product) throws NotFoundException, BusinessServiceException {
        if(product == null) {
            throw new NotFoundException("product %s not found", productId);
        }
        if(quantity < 1) {
            throw new BusinessServiceException("Quantity value %s not valid", quantity);
        }
        if(!isQuantityAvailableInStock(product, quantity)) {
            throw new BusinessServiceException("Quantity %s asked for the product %s is not available in stock", quantity, productId);
        }
    }

    private boolean isQuantityAvailableInStock(Product product, Integer quantity) {
        return product.getQuantityAvailable() >= quantity;
    }

    private Double calculateLineAmount(Product product, Integer quantity) {
        return product.getUnitPrice() * quantity;
    }

    private Long getNextSequenceValue() {
        return itemSequenceGenerator.incrementAndGet();
    }
}
