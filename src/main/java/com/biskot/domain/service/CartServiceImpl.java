package com.biskot.domain.service;

import com.biskot.domain.model.Cart;
import com.biskot.domain.model.Item;
import com.biskot.domain.model.Product;
import com.biskot.domain.service.exception.NotFoundException;
import com.biskot.domain.service.exception.BusinessServiceException;
import com.biskot.infra.repository.InMemoryCartRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class CartServiceImpl implements CartService {

    @Value("${biskot.cart.max}")
    private Double maxCartAmount;

    @Value("${biskot.product.max}")
    private Integer maxProductCount;

    private final InMemoryCartRepository cartRepository;
    private final ItemService itemService;

    public CartServiceImpl(InMemoryCartRepository cartRepository, ItemService itemService) {
        this.cartRepository = cartRepository;
        this.itemService = itemService;
    }

    @Override
    public void createCart() {
        Cart newCart = new Cart()
                .id(cartRepository.getNextSequenceValue());
        cartRepository.saveCart(newCart);
    }

    @Override
    public Cart getCart(long cartId) {
        return cartRepository.getCart(cartId).orElseThrow(NotFoundException::new);
    }

    @Override
    public void addItemToCart(long cartId, long productId, int quantityToAdd) {
        Cart cart = getCart(cartId);
        Item newItem = itemService.createItem(productId, quantityToAdd);
        businessRulesValidation(cart, newItem);
        cart.addItem(newItem);
        calculateTotalPrice(cart, newItem);
        cartRepository.saveCart(cart);
    }

    @Override
    public void initDatabase() {
        cartRepository.initDataSource();
    }

    private void businessRulesValidation(Cart cart, Item newItem) throws BusinessServiceException {
        if(productCountExceed(newItem, cart) ) {
            throw new BusinessServiceException("product quota %s has been exceed", maxProductCount);
        }
        if(amountExceed(newItem, cart) ) {
            throw new BusinessServiceException("cart max amount %s has been exceed", maxCartAmount);
        }
    }

    private void calculateTotalPrice(Cart cart, Item newItem) {
        double total = cart.getTotalPrice() + newItem.getLineAmount();
        cart.setTotalPrice(total);
    }

    private boolean productCountExceed(Item item, Cart cart) {
        List<Product> products = cart.getItems().stream()
                .map(productItem -> productItem.getProduct())
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        products.add(item.getProduct());
        return  (products.stream().collect(Collectors.groupingBy(Product::getId)).size()) > maxProductCount;
    }

    private boolean amountExceed(Item item, Cart cart) {
        return cart.getTotalPrice() + item.getLineAmount() > maxCartAmount;
    }
}
