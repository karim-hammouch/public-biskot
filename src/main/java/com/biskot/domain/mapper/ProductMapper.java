package com.biskot.domain.mapper;

import com.biskot.domain.model.Product;
import com.biskot.infra.gateway.payload.ProductResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface ProductMapper extends EntityMapper<ProductResponse, Product> {

    @Mapping(source = "quantityInStock", target = "quantityAvailable")
    Product toEntity(ProductResponse dto);

    @Mapping(target = "quantityInStock", source = "quantityAvailable")
    ProductResponse toDto(Product entity);
}
