package com.biskot.domain.mapper.output;

import com.biskot.app.contract.model.ItemResponse;
import com.biskot.domain.mapper.EntityMapper;
import com.biskot.domain.model.Item;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface ItemResponseMapper extends EntityMapper<ItemResponse, Item> {

    @Override
    @Mapping(source = "productId", target = "product.id")
    @Mapping(source = "productLabel", target = "product.label")
    @Mapping(source = "quantity", target = "product.quantityAvailable")
    @Mapping(source = "unitPrice", target = "product.unitPrice")
    @Mapping(source = "linePrice", target = "lineAmount")
    Item toEntity(ItemResponse dto);

    @Override
    @Mapping(target = "productId", source = "product.id")
    @Mapping(target = "productLabel", source = "product.label")
    @Mapping(target = "quantity", source = "product.quantityAvailable")
    @Mapping(target = "unitPrice", source = "product.unitPrice")
    @Mapping(target = "linePrice", source = "lineAmount")
    ItemResponse toDto(Item entity);
}
