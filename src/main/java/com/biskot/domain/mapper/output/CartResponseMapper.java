package com.biskot.domain.mapper.output;

import com.biskot.app.contract.model.CartResponse;
import com.biskot.domain.mapper.EntityMapper;
import com.biskot.domain.model.Cart;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {ItemResponseMapper.class})
public interface CartResponseMapper extends EntityMapper<CartResponse, Cart> {
}
