package com.biskot.domain.mapper;

import com.biskot.domain.model.Item;
import com.biskot.infra.repository.entity.ItemEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {ProductMapper.class})
public interface ItemMapper extends EntityMapper<Item, ItemEntity> {

    @Mapping(source = "product.id", target = "productId")
    ItemEntity toEntity(Item dto);

    @Mapping(source = "productId", target = "product.id")
    Item toDto(ItemEntity entity);
}
