package com.biskot.domain.mapper;

import com.biskot.domain.model.Cart;
import com.biskot.infra.repository.entity.CartEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {ItemMapper.class})
public interface CartMapper extends EntityMapper<Cart, CartEntity> {
}
