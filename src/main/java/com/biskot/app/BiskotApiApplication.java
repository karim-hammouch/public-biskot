package com.biskot.app;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@SpringBootApplication(scanBasePackages = "com.biskot")
@Configuration
public class BiskotApiApplication {

	@Value("${biskot.gateway.mock.server.port}")
	private Integer mockPort;

	@Bean
	public WebClient localMockApiClient() {
		return WebClient.create("http://localhost:" + mockPort);
	}

	public static void main(String[] args) {
		SpringApplication.run(BiskotApiApplication.class, args);
	}

}
