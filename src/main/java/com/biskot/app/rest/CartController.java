package com.biskot.app.rest;

import com.biskot.app.contract.api.CartApi;
import com.biskot.app.contract.model.AddItemRequest;
import com.biskot.app.contract.model.CartResponse;
import com.biskot.domain.mapper.output.CartResponseMapper;
import com.biskot.domain.model.Cart;
import com.biskot.domain.service.CartService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CartController implements CartApi {

    private final CartService cartService;

    private final CartResponseMapper cartResponseMapper;

    public CartController(CartService cartService, CartResponseMapper cartResponseMapper) {
        this.cartService = cartService;
        this.cartResponseMapper = cartResponseMapper;
    }

    @Override
    public ResponseEntity<Void> addItemToCart(Long cartId, AddItemRequest addItemRequest) {
        cartService.addItemToCart(cartId, addItemRequest.getProductId(), addItemRequest.getQuantity());
        return ResponseEntity
                .ok()
                .build();
    }

    @Override
    public ResponseEntity<Void> createCart() {
        cartService.createCart();
        return ResponseEntity
                .ok()
                .header("message", "cart created")
                .build();
    }

    @Override
    public ResponseEntity<CartResponse> getCart(Long cartId) {
        Cart cart = cartService.getCart(cartId);
        return ResponseEntity
                .ok()
                .body(cartResponseMapper.toDto(cart));
    }
}
